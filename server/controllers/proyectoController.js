const Proyecto = require('../models/Proyecto');

exports.crearProyecto = async (req, res) => {
    try {
        //Crear un nuevo proyecto
        const proyecto = new Proyecto(req.body);
 
        //Guardar creador via JWT
        proyecto.creador = req.usuario.id;

        //Guardamos el proyecto
        proyecto.save();

        res.json(proyecto);

    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error' });
    }
}